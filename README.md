TODO-app
========
En enkel applikation för hantering av aktiviteter som behöver genomföras. Applikationen saknar GUI och har endast ett API. Detta API följer REST-principerna och använder JSON som datakodning.

Använda
-------
För att ansluta till API:et föreslås modulen [requests](http://docs.python-requests.org/en/latest/).
Alla data som skickas till API:et måste skickas som JSON med HTTP-headern `content-type: application/json`.

Metoderna som förändrar data kräver autentisering (`POST`, `PUT`, `DELETE`).

### Exempel-data
Flera uppgifter formateras på följande vis:

	mac:~ linus$ curl -i http://example.com/todo/api/1.0/tasks
	HTTP/1.1 200 OK
	Server: nginx/1.2.1
	Date: Tue, 01 Apr 2014 14:20:53 GMT
	Content-Type: application/json
	Content-Length: 415
	
	{
	  "tasks": [
	    {
	      "description": "Read A-byte-of-python to get a good introduction to Python", 
	      "done": true, 
	      "title": "Get introduced to Python", 
	      "uri": "http://api.t-net.se/todo/api/1.0/tasks/1"
	    }, 
	    {
	      "description": "Visit the Python website.", 
	      "done": false, 
	      "title": "Python website", 
	      "uri": "http://api.t-net.se/todo/api/1.0/tasks/2"
	    }
	  ]
	}

Enstaka uppgifter formateras på följande vis:

	mac:~ linus$ curl -i http://api.t-net.se/todo/api/1.0/tasks/2
	HTTP/1.1 200 OK
	Server: nginx/1.2.1
	Date: Tue, 01 Apr 2014 14:23:56 GMT
	Content-Type: application/json
	Content-Length: 174
	
	{
	  "task": {
	    "description": "Visit the Python website.", 
	    "done": false, 
	    "title": "Python website", 
	    "uri": "http://api.t-net.se/todo/api/1.0/tasks/2"
	  }
	}

### Lista alla tasks.
Använd HTTP-metoden `GET` och URIn `/tasks` för att hämta alla uppgifter. De returnes som en lista, `tasks`.

	requests.get(url + '/tasks')

### Hämta en task.
Använd HTTP-metoden `GET` och URIn `/tasks/<int>` för att hämta en specifik uppgift. Den returneras som objektet `task`. 

	requests.get(url + '/tasks/42')

### Skapa en ny task.
Använd HTTP-metoden `POST` och URIn `/tasks` för att skapa en ny uppgift. HTTP-requesten måste skicka med ett JSON-kodat objekt med attributet title. Den nya uppgiften returneras som objektet `task`.

	data = {'title': 'Måla huset', 'description': 'Använd blå färg.'}
	requests.post(url + '/tasks', 
	              data = json.dumps(data),
	              headers = {'content-type': 'application/json'},
	              auth = ('admin', 'secret'))

### Ändra en task.
Använd HTTP-metoden `PUT` och URIn `/tasks/<int>` för att uppdatera en uppgift. HTTP-requesten måste skicka med ett JSON-kodat objekt med de attribut som ska ändras. Den uppdaterade uppgiften returneras som objektet `task`.

	data = {'done': True}
	requests.put(url + '/tasks/42', 
	             data = json.dumps(data),
	             headers = {'content-type': 'application/json'},
	             auth = ('admin', 'secret'))

### Radera en task.
Använd HTTP-metoden `DELETE` och URIn `/tasks/<int>` för att radera en uppgift. Attributet `result` returneras med värdet `True` om det gick att genomföra.

	requests.delete(url + '/tasks/42',
	                auth = ('admin', 'secret'))

Installera
----------

### Installera körmiljö
För att installera de moduler som applikationen är beroende av kör följande kommandon.

	virtualenv venv
	. venv/bin/activate
	pip install -r requirements.txt

### Starta applikationen lokalt
För att starta applikationen (se till att den virtuella miljön är aktiv).

	./run.py

### På en server
För att köra applikationen på en server rekommenderas [uWSGI](https://uwsgi-docs.readthedocs.org/en/latest/) och [NGINX](http://nginx.org/).
Det finns konfigurationsfiler för att köra dessa på en [Debian-server](https://www.debian.org/index.sv.html) i mappen conf.